package StNetYourchenkov;

public class Car extends Propperty {
    private int maxSpeed;
    private double vechicleVol;
    private String transmission;
    private String tankType;

    public Car(String coler, String vendor, int price) {
        super(coler, vendor, price);
    }

    public Car(String coler, String vendor, int price, int maxSpeed, double vechicleVol, String transmission, String tankType) {
        super(coler, vendor, price);
        this.maxSpeed = maxSpeed;
        this.vechicleVol = vechicleVol;
        this.transmission = transmission;
        this.tankType = tankType;
    }

    public int getMaxSpeed() {
        return maxSpeed;
    }

    public void setMaxSpeed(int maxSpeed) {
        this.maxSpeed = maxSpeed;
    }

    public double getVechicleVol() {
        return vechicleVol;
    }

    public void setVechicleVol(double vechicleVol) {
        this.vechicleVol = vechicleVol;
    }

    public String getTransmission() {
        return transmission;
    }

    public void setTransmission(String transmission) {
        this.transmission = transmission;
    }

    public String getTankType() {
        return tankType;
    }

    public void setTankType(String tankType) {
        this.tankType = tankType;
    }

    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder("StNetYourchenkov.Car{");
        sb.append("maxSpeed=").append(maxSpeed);
        sb.append(", vechicleVol=").append(vechicleVol);
        sb.append(", transmission='").append(transmission).append('\'');
        sb.append(", tankType='").append(tankType).append('\'');
        sb.append(", coler='").append(coler).append('\'');
        sb.append(", vendor='").append(vendor).append('\'');
        sb.append(", price=").append(price);
        sb.append('}');
        return sb.toString();
    }

    @Override
    public void getInfo() {
        System.out.println(toString());
    }
}
