package StNetYourchenkov;


public  abstract class Propperty {
    protected String coler;
    protected String vendor;
    protected int price;



    public Propperty(String coler, String vendor, int price) {
        this.coler = coler;
        this.vendor = vendor;
        this.price = price;
    }

    public String getColer() {
        return coler;
    }

    public void setColer(String coler) {
        this.coler = coler;
    }

    public String getVendor() {
        return vendor;
    }

    public void setVendor(String vendor) {

        this.vendor = vendor;
    }

    public int getPrice() {
        return price;
    }

    public void setPrice(int price) {
        this.price = price;
    }

    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder("StNetYourchenkov.Propperty{");
        sb.append("coler='").append(coler).append('\'');
        sb.append(", vendor='").append(vendor).append('\'');
        sb.append(", price=").append(price);
        sb.append('}');
        return sb.toString();
    }
    public abstract   void getInfo();
}
